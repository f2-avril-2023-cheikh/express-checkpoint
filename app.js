var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var serviceRouter = require("./routes/service");
var contactRouter = require("./routes/contact");

var app = express();

const heureDeFermeture = (req, res, next) => {
  const now = new Date();
  const day = now.getDay(); // 0 (dimanche) à 6 (samedi)
  const hour = now.getHours(); // 0 à 23 (format 24 heures)

  if (day >= 1 && day <= 5 && hour >= 10 && hour <= 18) {
    next();
  } else {
    res
      .status(503)
      .send(
        "Désolé, le service est disponible Uniquements aux heures d'ouverture (10 heure a 18 heure) du lundi au vendredi "
      );
  }
};
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(heureDeFermeture);
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/service", serviceRouter);
app.use("/contact", contactRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
